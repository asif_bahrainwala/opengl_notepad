#ifndef SMOBJ
#define SMOBJ

#include <string>
#include <string.h>
#include <iostream>
#include <vector>
#include <memory>
#include <sstream>
#include <fstream>
#include <stdlib.h>
#include <cmath>
#include <chrono>
using namespace std;

#define GL_GLEXT_PROTOTYPES

#include <GL/glx.h>
#include <GL/gl.h>
#include <GL/glext.h>

#include "soil/src/SOIL.h"

#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp>

#define WIDTH   800
#define HEIGHT  500

extern float fontwidth;
extern float fontheight;

class Texture
{
    GLuint m_textureId=0;

public:
    Texture(GLuint textureId):m_textureId(textureId){}
    Texture(int w,int h)
    {
        glGenTextures(1,&m_textureId);
        glBindTexture(GL_TEXTURE_2D, m_textureId);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        auto m1=max(w,h);
        auto mipmaplevelrequired=log(m1)/log(2); //taking log to the base 2
        glTexStorage2D(GL_TEXTURE_2D, mipmaplevelrequired, GL_RGBA8, w, h);
    }
    Texture(const string &s)
    {
        m_textureId=SOIL_load_OGL_texture(s.c_str(),SOIL_LOAD_AUTO,SOIL_CREATE_NEW_ID,SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
    }

    GLuint GetID() {return m_textureId;}

    void UseTexture(GLenum stage=GL_TEXTURE0)
    {
        glActiveTexture(stage);
        glBindTexture(GL_TEXTURE_2D, m_textureId);
    }

    string SaveToFile()
    {
        int w=0,h=0;
        glBindTexture(GL_TEXTURE_2D, m_textureId);
        glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH,  &w);glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &h);
        unsigned char data[w*h*3]={};
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1); //https://www.khronos.org/opengl/wiki/Common_Mistakes
        glGetTexImage(GL_TEXTURE_2D,0, GL_RGB,GL_UNSIGNED_BYTE,(GLvoid*)data);
        stringstream ss;
        ss<<this<<"_image.bmp";
        SOIL_save_image(ss.str().c_str(),SOIL_SAVE_TYPE_BMP,w,h,3,data);

        return ss.str();
    }

    ~Texture()
    {
        glDeleteTextures(1,&m_textureId);
    }
};

class Program
{
    GLuint m_programID = glCreateProgram();

    static GLuint shaderCompileFromFile(GLenum type,const char *source);
    void shaderAttachFromFile(GLenum type, const char *shaderSource);
    void GetLinkStatus();

public:
    void setShaderSource(const char *vertexShaderSrc,const char *HullShaderSrc,const char *DomainShaderSrc,const char *geomShaderSrc,const char *fragmentShaderSrc)
    {
        shaderAttachFromFile(GL_VERTEX_SHADER,vertexShaderSrc);
        shaderAttachFromFile(GL_TESS_CONTROL_SHADER,HullShaderSrc);
        shaderAttachFromFile(GL_TESS_EVALUATION_SHADER,DomainShaderSrc);
        shaderAttachFromFile(GL_GEOMETRY_SHADER,geomShaderSrc);
        shaderAttachFromFile(GL_FRAGMENT_SHADER,fragmentShaderSrc);

        glLinkProgram(m_programID);

        GetLinkStatus();
    }

    ~Program()
    {
        glDeleteProgram(m_programID);
    }

    GLuint GetID(){return m_programID;}
};

struct MyVertex
{
    MyVertex(){}
    MyVertex(float i,float j,float k,float ni,float nj,float nk,float l,float m):
        x(i),y(j),z(k),
        nx(ni),ny(nj),nz(nk),
        tx(l),ty(m)
    {}
    float x, y, z;        //Vertex
    float nx, ny, nz;     //Normal
    float tx,ty;          //for texture stage 1
};

struct Geom
{
    GLuint m_vertex=0;
    GLuint m_calllist=glGenLists(1);
    bool m_calllistCreated=false;
    shared_ptr<Program> m_program=0;
    vector<shared_ptr<Texture>> m_tex;
    vector<MyVertex> m_vertexPoints;

    virtual ~Geom(){
        glDeleteLists(m_calllist,1);
        glDeleteBuffers(1,&m_vertex);
    }
    virtual void Draw(ssize_t character,glm::mat4=glm::identity<glm::mat4>(),int blink=0,float alpha = 1);
};

#endif
