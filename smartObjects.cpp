#include "smartObjects.h"

GLuint Program::shaderCompileFromFile(GLenum type,const char *source)
{
    GLuint shader;
    GLint  result;

    /* create shader object, set the source, and compile */
    shader = glCreateShader(type);
    GLint length = strlen(source);
    glShaderSource(shader, 1, (const char **)&source, &length);
    glCompileShader(shader);

    /* make sure the compilation was successful */
    glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
    if(result == GL_FALSE) {
        char *log;
        /* get the shader info log */
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
        log = (char*)malloc(length);
        glGetShaderInfoLog(shader, length, &result, log);

        /* print an error message and the info log */
        printf("shaderCompileFromFile(): Unable to compile %s: %s\n",source,log);
        free(log);

        glDeleteShader(shader);
        return 0;
    }

    return shader;
}

void Program::shaderAttachFromFile(GLenum type, const char *shaderSource)
{
    if(nullptr==shaderSource) return;

    /* compile the shader */
    GLuint shader = shaderCompileFromFile(type, shaderSource);
    if(shader != 0) {
        glAttachShader(m_programID, shader);     /* attach the shader to the program */

        /* delete the shader - it won't actually be
         * destroyed until the program that it's attached
         * to has been destroyed */
        glDeleteShader(shader);
        shader=0;
    }
}

void Program::GetLinkStatus()
{
    GLint result=0;glGetProgramiv(m_programID, GL_LINK_STATUS, &result);
    if(result == GL_FALSE) {
        GLint length;
        char *log;
        /* get the program info log */
        glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &length);
        log = (char*)malloc(length);
        glGetProgramInfoLog(m_programID, length, &result, log);
        /* print an error message and the info log */
        fprintf(stderr, "Program linking failed: %s\n", log);
        free(log);
    }
}

void Geom::Draw(ssize_t character,glm::mat4 mat,int blink,float alpha)
{
    if(character<0 || character>=m_tex.size()) return;

    auto progID=m_program->GetID();

    glUseProgram(progID); //use the shaders

    static auto start = chrono::steady_clock::now();
    auto end = chrono::steady_clock::now();
    std::chrono::duration<double> diff = end-start;

    GLint gli=0;
    gli=glGetUniformLocation(progID, "timestep");
    glUniform1i(gli, diff.count()*2);

    gli=glGetUniformLocation(progID, "blink");
    glUniform1i(gli, blink);

    gli = glGetUniformLocation(progID, "MVP");
    glUniformMatrix4fv(gli, 1, GL_FALSE, &mat[0][0]);

    gli = glGetUniformLocation(progID, "alpha");
    glUniform1f(gli,alpha);

    m_tex[character]->UseTexture(GL_TEXTURE0);

    if(false == m_calllistCreated)
    {
        m_calllistCreated=true;
        glNewList(m_calllist,GL_COMPILE_AND_EXECUTE);  //create a call list

        glBindBuffer(GL_ARRAY_BUFFER, m_vertex);
        GLint posAttrib = glGetAttribLocation(progID, "pos");
        glEnableVertexAttribArray(posAttrib);
        glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(MyVertex), 0);

        posAttrib = glGetAttribLocation(progID, "normal");
        glEnableVertexAttribArray(posAttrib);
        glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(MyVertex),(void*)(3*sizeof(float)));

        posAttrib = glGetAttribLocation(progID, "tex");
        glEnableVertexAttribArray(posAttrib);
        glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(MyVertex),(void*)(6*sizeof(float)));

        gli=glGetUniformLocation(progID, "colorMap0");
        glUniform1i(gli, 0);

        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND);

        glDrawArrays( GL_TRIANGLES, 0, m_vertexPoints.size() );  //draw the frame

        glDisable(GL_BLEND);
        glEndList();   //done creating the call list
    }
    else
    {
        glCallList(m_calllist);
    }
    glUseProgram(0); //use the shaders


}
