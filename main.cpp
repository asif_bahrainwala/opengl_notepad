//create images from fonts via imagemagic
//convert -background none -font /usr/share/fonts/truetype/ubuntu/Ubuntu-BI.ttf  -fill black  -size "140x240"  label:"a" +repage -depth 8 a.png

#include <iostream>
#include "GLFWWindow.h"
#include "Geometry.h"
using namespace std;

float fontwidth=.05/2;
float fontheight=.1/2;

void drawScanCode(glm::vec3 position,Geometry::Frame &f,int keycode=0,int blink=0)
{
    glm::mat4 mat=glm::identity<glm::mat4>();
    mat = glm::scale(mat,glm::vec3(fontwidth,fontheight,1));

    glm::mat4 mat1=glm::identity<glm::mat4>();
    mat1 = glm::translate(mat1,glm::vec3(position));

    mat=mat1*mat;
    if(keycode>=65 && keycode<92)
        f.Draw(keycode-65,mat,blink,.8);
    else
    {
        //special cases
        switch(keycode)
        {
        case 32:
            f.Draw(26,mat,blink,.8);
            break;
        case 44:
            f.Draw(27,mat,blink,.8);
            break;
        case 46:
            f.Draw(28,mat,blink,.8);
            break;
        }
    }
}

int main()
{
    OpenGLWindow w;

    Geometry::Frame background;
    background.m_tex.push_back(make_shared<Texture>("../opengl_notepad/fish.png"));
    Geometry::Frame f;
    f.loadAllAlphabets("../opengl_notepad/ABC/");

    Geometry::Frame backgroundgothic;
    backgroundgothic.m_tex.push_back(make_shared<Texture>("../opengl_notepad/castle.png"));
    Geometry::Frame fgothic;
    fgothic.loadAllAlphabets("../opengl_notepad/Gothic/");

    shared_ptr<Program> pCommon(new Program());
    pCommon->setShaderSource(Geometry::vertexShaderSrc,nullptr,nullptr,nullptr,Geometry::fragmentShaderSrc);
    backgroundgothic.m_program = fgothic.m_program = background.m_program = f.m_program = pCommon;

    glClearDepth(1.0f);   //is always to set to 1.0 when clearing
    //glEnable(GL_DEPTH_TEST);  //we dont need depth test

    while (!glfwWindowShouldClose(w.m_window))
    {
        glClearColor( 1, 1, 1, 0.0f );
        glClear( GL_COLOR_BUFFER_BIT |GL_DEPTH_BUFFER_BIT);

        if(OpenGLWindow::cycleFont%2)
            background.Draw(0);
        else
            backgroundgothic.Draw(0);

        float y=0;
        int n=0;
        int i1=0;
        for(size_t i=0;i<OpenGLWindow::keycodelist.size();++i)
        {
            float x=(fontwidth+i1*fontwidth*2-1);i1++;

            int numberOfCharactersIn1Row=40*((.05/2)/fontwidth)+1;
            if(numberOfCharactersIn1Row == i1) i1=0;

            float y=(i)/numberOfCharactersIn1Row;
            y=y*-fontheight*2;

            glm::vec4 position(x,y+.9 - OpenGLWindow::mouseYWheel/50,0,1);

            if(OpenGLWindow::cycleFont%2)
                drawScanCode(position,f,OpenGLWindow::keycodelist[i],i==OpenGLWindow::currentCursorPosition);
            else
                drawScanCode(position,fgothic,OpenGLWindow::keycodelist[i],i==OpenGLWindow::currentCursorPosition);
        }

        w.OpenGLWindowSwapBuffers();
        glfwPollEvents();
    }
    return 0;
}
