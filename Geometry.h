#ifndef GEOMETRY_H
#define GEOMETRY_H

namespace Geometry
{

#define GLSL(src) "#version 300 es\n" #src
const char* vertexShaderSrc = GLSL(
in vec3 pos;
in vec3 normal;   //we may use this later
in vec2 tex;

uniform mat4 MVP;
out vec2 texOut;

void main() {
    gl_Position.xyz = pos.xyz;
    gl_Position.w=1.0f;
    gl_Position=MVP*gl_Position;
    texOut=tex;
}
);

const char* fragmentShaderSrc = GLSL(

precision mediump float;
uniform sampler2D colorMap0;
uniform int timestep;
uniform int blink;
uniform float alpha;
in vec2 texOut;
out vec4 FragColor;

void main() {
    if(0==blink)
    {
        FragColor   = texture(colorMap0, texOut.xy);
        FragColor.a = alpha;
        return;
    }

    //make it blink
    if(timestep %2 == 0)
        FragColor   = vec4(1.0,1.0,1.0,1.0)-texture(colorMap0, texOut.xy);
    else
        FragColor   = texture(colorMap0, texOut.xy);

    FragColor.a=.5; //add alpha belending to provide transparency
}
);

struct Frame:Geom
{
    Frame()
    {
        m_vertexPoints.resize(6);

        m_vertexPoints[0]=MyVertex(-1,-1,  0,  0,0,1, 0,0);
        m_vertexPoints[1]=MyVertex(1 , 1,  0,  0,0,1, 1,1);
        m_vertexPoints[2]=MyVertex(-1  ,1, 0,  0,0,1, 0,1);

        m_vertexPoints[3]=MyVertex(-1  ,-1, 0, 0,0,1,  0,0);
        m_vertexPoints[4]=MyVertex(1  ,1, 0,   0,0,1,  1,1);
        m_vertexPoints[5]=MyVertex(1  ,-1, 0,  0,0,1,  1,0);

        glGenBuffers(1, &m_vertex);
        glBindBuffer(GL_ARRAY_BUFFER, m_vertex);
        glBufferData(GL_ARRAY_BUFFER, m_vertexPoints.size()*sizeof(MyVertex), &m_vertexPoints[0], GL_STATIC_DRAW);
    }


    void loadAllAlphabets(const string &s)
    {
        m_tex.push_back(make_shared<Texture>(s+"a.png"));
        m_tex.push_back(make_shared<Texture>(s+"b.png"));
        m_tex.push_back(make_shared<Texture>(s+"c.png"));
        m_tex.push_back(make_shared<Texture>(s+"d.png"));
        m_tex.push_back(make_shared<Texture>(s+"e.png"));
        m_tex.push_back(make_shared<Texture>(s+"f.png"));
        m_tex.push_back(make_shared<Texture>(s+"g.png"));
        m_tex.push_back(make_shared<Texture>(s+"h.png"));
        m_tex.push_back(make_shared<Texture>(s+"i.png"));
        m_tex.push_back(make_shared<Texture>(s+"j.png"));
        m_tex.push_back(make_shared<Texture>(s+"k.png"));
        m_tex.push_back(make_shared<Texture>(s+"l.png"));
        m_tex.push_back(make_shared<Texture>(s+"m.png"));
        m_tex.push_back(make_shared<Texture>(s+"n.png"));
        m_tex.push_back(make_shared<Texture>(s+"o.png"));
        m_tex.push_back(make_shared<Texture>(s+"p.png"));
        m_tex.push_back(make_shared<Texture>(s+"q.png"));
        m_tex.push_back(make_shared<Texture>(s+"r.png"));
        m_tex.push_back(make_shared<Texture>(s+"s.png"));
        m_tex.push_back(make_shared<Texture>(s+"t.png"));
        m_tex.push_back(make_shared<Texture>(s+"u.png"));
        m_tex.push_back(make_shared<Texture>(s+"v.png"));
        m_tex.push_back(make_shared<Texture>(s+"w.png"));
        m_tex.push_back(make_shared<Texture>(s+"x.png"));
        m_tex.push_back(make_shared<Texture>(s+"y.png"));
        m_tex.push_back(make_shared<Texture>(s+"z.png"));
        m_tex.push_back(make_shared<Texture>(s+"space.png"));
        m_tex.push_back(make_shared<Texture>(s+"comma.png"));
        m_tex.push_back(make_shared<Texture>(s+"dot.png"));
    }
};
}

#endif // GEOMETRY_H
