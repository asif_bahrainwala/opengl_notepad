TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        GLFWWindow.cpp \
        main.cpp \
        smartObjects.cpp

HEADERS += \
    GLFWWindow.h \
    Geometry.h \
    smartObjects.h

LIBS+= -lglfw -lGL -lpthread


SOURCES += soil/src/SOIL.c
SOURCES += soil/src/stb_image_aug.c
SOURCES += soil/src/image_helper.c
SOURCES += soil/src/image_DXT.c

INCLUDEPATH += glm
