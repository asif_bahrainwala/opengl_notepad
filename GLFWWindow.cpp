#include "GLFWWindow.h"

void resetGLViewport(GLFWwindow *window)
{
    int width=0;
    int height=0;
    glfwGetWindowSize(window, &width, &height);
    glViewport(0,0,width,height);  //warning FBs are not resized
}

vector<int> OpenGLWindow::keycodelist;
double OpenGLWindow::mouseYWheel=0;
ssize_t OpenGLWindow::currentCursorPosition=-1;
size_t OpenGLWindow::cycleFont=0;

void OpenGLWindow::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){

    cout<<key<<" "<<scancode<<" "<<action<<" "<<mods<<" "<<OpenGLWindow::keycodelist.size()<<endl;

    if(action>0) return;

    if(84==key && 2==mods)
    {
        ++OpenGLWindow::cycleFont;
        return;
    }

    if((334 ==key || 61==key) && 2==mods)
    {
        //zoom in
        if (fontwidth<=.1) fontwidth*=2;
        if (fontheight<=.2)fontheight*=2;
        return;
    }
    else if((333 ==key || 45==key) && 2==mods)
    {
        //zoom out
        if (fontwidth>=.05/4) fontwidth/=2;
        if (fontheight>=.1/4) fontheight/=2;
        return;
    }

    if(263==key)
    {//going left
        if(OpenGLWindow::currentCursorPosition>0)
            --OpenGLWindow::currentCursorPosition;
        return;
    }

    if(262==key)
    {//going right
        if(OpenGLWindow::currentCursorPosition<(ssize_t)OpenGLWindow::keycodelist.size()-1)
            ++OpenGLWindow::currentCursorPosition;
        return;
    }

    if(265==key)
    {//going up
        int numberOfCharactersIn1Row=40*((.05/2)/fontwidth)+1;

        if(OpenGLWindow::currentCursorPosition>=(ssize_t)numberOfCharactersIn1Row)
            OpenGLWindow::currentCursorPosition-=numberOfCharactersIn1Row;
        return;
    }
    if(264==key)
    {//going down
        int numberOfCharactersIn1Row=40*((.05/2)/fontwidth)+1;

        if(OpenGLWindow::currentCursorPosition+(ssize_t)numberOfCharactersIn1Row<OpenGLWindow::keycodelist.size())
            OpenGLWindow::currentCursorPosition+=numberOfCharactersIn1Row;
        return;
    }

    if(22==scancode)  //back space
    {
        if(OpenGLWindow::currentCursorPosition == (ssize_t)OpenGLWindow::keycodelist.size()-1)
        {
            if(OpenGLWindow::keycodelist.size())
            {
                OpenGLWindow::keycodelist.pop_back();
                OpenGLWindow::currentCursorPosition=OpenGLWindow::keycodelist.size()-1;
            }
        }
        else{
                if(OpenGLWindow::keycodelist.begin()+OpenGLWindow::currentCursorPosition!=OpenGLWindow::keycodelist.end())
                {
                    OpenGLWindow::keycodelist.erase(OpenGLWindow::keycodelist.begin()+OpenGLWindow::currentCursorPosition);
                    OpenGLWindow::currentCursorPosition--;
                    OpenGLWindow::currentCursorPosition=max(ssize_t(0),OpenGLWindow::currentCursorPosition);
                    return;
                }
        }
    }
    else if(key<255)
    {
        if(OpenGLWindow::currentCursorPosition == (ssize_t)OpenGLWindow::keycodelist.size()-1)
        {
            OpenGLWindow::keycodelist.push_back(key);
            OpenGLWindow::currentCursorPosition=OpenGLWindow::keycodelist.size()-1;
        }
        else {
                OpenGLWindow::keycodelist.insert(OpenGLWindow::keycodelist.begin()+OpenGLWindow::currentCursorPosition+1,key);
                OpenGLWindow::currentCursorPosition++;
                return;
        }
    }

}

void OpenGLWindow::mouse_callback(GLFWwindow* window, double xpos, double ypos){

}

void OpenGLWindow::window_resize(GLFWwindow *window,int,int)
{
    resetGLViewport(window);
}

void OpenGLWindow::mouse_wheel(GLFWwindow*,double a,double b)
{
    //cout<<"wheel"<<a<<" "<<b<<endl<<flush;
     OpenGLWindow::mouseYWheel+=b;
}
