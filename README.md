# README #

The source requires qmake build system

Install
sudo apt-get install qtcreator qt5-default
sudo apt-get install libglfw3-dev 

### What is this repository for? ###

* A simple notepad , text rendering via OpenGL 3+

### Supports:
Built using OpenGL (uses 3.00 ES core for shader compilation)
Left/Right/up/down Arrow key to move cursor
Use mouse wheel to scroll
insert/delete from where cursor is blinking
Use CTRL+ T to toggle between the 2 fonts
Use CTRL+ + to zoom+
Use CTRL+ - to zoom- 

### Tested on:
Linux Mint 20

