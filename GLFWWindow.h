#ifndef GLFWWINDOW_H
#define GLFWWINDOW_H

#include "smartObjects.h"
#include <GLFW/glfw3.h>
using namespace std;

struct OpenGLWindow
{
    GLFWwindow *m_window;
    OpenGLWindow()
    {
        if (!glfwInit()) exit(-1);

        m_window = glfwCreateWindow(WIDTH, HEIGHT, "Hello World", NULL, NULL);
        glfwSetKeyCallback(m_window, key_callback);
        glfwSetCursorPosCallback(m_window, mouse_callback);
        glfwSetScrollCallback(m_window,mouse_wheel);
        glfwSetWindowSizeCallback(m_window,window_resize);
        glfwMakeContextCurrent(m_window);
    }

    ~OpenGLWindow()
    {
        glfwDestroyWindow(m_window);
        glfwTerminate();
    }

    void OpenGLWindowSwapBuffers(){glfwSwapBuffers(m_window);}

    static void window_resize(GLFWwindow*,int,int);
    static void mouse_callback(GLFWwindow* window, double xpos, double ypos);
    static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
    static void mouse_wheel(GLFWwindow*,double,double);
    static vector<int> keycodelist;
    static double mouseYWheel;
    static ssize_t currentCursorPosition;
    static size_t cycleFont;
};

#endif // GLFWWINDOW_H
